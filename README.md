# Bindings for libxlsxwriter

Currently the libxlsxwriter library (for Linux x64) is included in the repo. Which is ugly and of limited use. This will be fixed whenever I get dub to first compile the C code before compiling the D code.

# Platforms

libxlsxwriter works only on Linux, FreeBSD, OS X and iOS. And is FreeBSD Licensed.

# Example

```
import dexcel.c.xlsxwriter;

void main()
{
    import std.string;
    lxw_workbook* workbook  = new_workbook(cast(char*)"myexcel.xlsx".toStringz);
    lxw_worksheet * worksheet = workbook_add_worksheet(workbook, null);
    uint row = 0;
    ushort col = 0;
    double ratio = 108/144.0;
    double emuPerInch = 914400;
    ulong width = cast(ulong)(emuPerInch * 2);
    ulong height = cast(ulong)(width * ratio);
    lxw_drawing *drawing = workbook_add_drawing(workbook,cast(char*)"name".toStringz,cast(char*)"descr".toStringz,cast(char*)"/app/test_image.jpeg".toStringz);
    worksheet_add_drawing(worksheet, drawing, 6u, 0u, 0u, 0u, width, height);
    worksheet_write_string(worksheet, row, col, cast(char*)"Hello me!".toStringz, null);
    workbook_close(workbook);
}
```

see [libxlsxwriter](http://libxlsxwriter.github.io/) for the complete API.